# Assignment Operator
a = 10
b = 5
print(a)
print(b)

print()

# Arithematic Operators
print(a + b)
print(a - b)
print(a * b)
print(a / b)
print(a % b)
print(a ** b)

print()

# Comparison Operators
print(a == b)
print(a != b)
print(a > b)
print(a < b)

print()
# && - and
# || - or
# ! - not

# Identity Operator -> "is"
print(a is b)

print()

# Membership Operator -> in
abc = [1, 2, 3, 4, 5]
print(b in abc)
print(a in abc)