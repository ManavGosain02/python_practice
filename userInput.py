# String Input
a = input("Enter a string: ")
print(type(a))
print("The string entered is: " + a)

print()

# Integer Input
a = int(input("Enter a number: "))
print(type(a))
print("The number entered is: ", a)

