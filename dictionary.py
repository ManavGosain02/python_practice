abc = {}
abc["name"] = "James"
abc["age"] = 43
print(abc)

xyz = {
    "name" : "James",
    "age" : 43,
    "gender" : "M",
}
print(xyz)


Employee = {
    "name" : "James",
    "age" : 22,
    "dob" : 2000,
    "empno" : 123
}
print(Employee)
Employee["empid"] = 123
Employee["age"] = 21
print(Employee)
Employee.pop("empid")
print(Employee)

Employee1 = {
    "name" : "James",
    "age" : 22,
    "dob" : 2000,
    "empno" : 123
}
Employee1["empid"] = 123

Employee2 = {
    "name" : "Cameron",
    "age" : 21,
    "dob" : 2001,
    "empno" : 124
}
Employee2.pop("dob")

Employees = {
    "emp1" : Employee1,
    "emp2" : Employee2
}

print(Employees)