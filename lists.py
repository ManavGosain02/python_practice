fruits = ["Apple", "Banana", "Mango", "Strawberry"]

print(fruits)
print(fruits[2])
print(fruits[2][3])
print(fruits[2][0:3])
print(fruits[2][-3:])

print(fruits.index("Strawberry"))

fruits[3] = "Orange"

print(fruits)

if "Mango" in fruits:
    print("Yes,  available")
else:
    print("No, not available")
    
print(len(fruits))

fruits.append("Strawberry")
print(fruits)
fruits.remove("Mango")
print(fruits)

numbers = ["12", "23", "34", "45"]

print(fruits + numbers)
print(fruits, numbers)

fruits.clear()
print(fruits)

fruits.extend(numbers)
print(fruits)