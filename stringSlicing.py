a = input("Enter a string: ")
print(a[3])
print(a[0:3])
print(a[-3:])
b = input("Enter another string: ")

print("The first input is: {} and the second input is: {}".format(a, b))
print("The second input is: {1} and the first input is: {0}".format(a, b))

print(len(a))

c = "       Hello world        "
print(c)
print(c.strip())
c = c.strip()
print(c)
print(c.lower())
print(c.upper())
print(c.title())

print(c.find("o"))
print(c.find("a"))

print(b.split(" "))

d = "Hello"
print(d.isnumeric())