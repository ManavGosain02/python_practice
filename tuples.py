fruits = ("Apple", "Banana", "Mango", "Strawberry")

print(fruits)
print(fruits[2])
print(fruits[2][3])
print(fruits[2][0:3])
print(fruits[2][-3:])

print(fruits.index("Strawberry"))

# fruits[3] = "Orange" tuples do not support item assignment

print(fruits)

if "Mango" in fruits:
    print("Yes,  available")
else:
    print("No, not available")
    
print(len(fruits))

# fruits.append("Strawberry") tuples do not support append
print(fruits)
# fruits.remove("Mango") tuples do not support remove
print(fruits)

numbers = ["12", "23", "34", "45"]

# print(fruits + numbers) tuples can not be concatenated

# fruits.clear() tuples do not support clear
print(fruits)

# fruits.extend(numbers) tuples do not support extend
print(fruits)